/*
Navicat SQLite Data Transfer

Source Server         : qomo
Source Server Version : 30808
Source Host           : :0

Target Server Type    : SQLite
Target Server Version : 30808
File Encoding         : 65001

Date: 2018-10-14 23:21:25
*/

PRAGMA foreign_keys = OFF;

-- ----------------------------
-- Table structure for messages
-- ----------------------------
DROP TABLE IF EXISTS "main"."messages";
CREATE TABLE "messages" ("id" integer primary key autoincrement,"created_at" datetime,"updated_at" datetime,"deleted_at" datetime,"key" varchar(255) NOT NULL,"user_id" integer,"note_key" varchar(255),"content" varchar(255),"praise" integer DEFAULT 0 );

-- ----------------------------
-- Records of messages
-- ----------------------------

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS "main"."notes";
CREATE TABLE "notes" ("id" integer primary key autoincrement,"created_at" datetime,"updated_at" datetime,"deleted_at" datetime,"key" varchar(255) NOT NULL,"user_id" integer,"title" varchar(255),"summary" text,"content" text,"files" text,"visit" integer DEFAULT 0,"praise" integer DEFAULT 0 );

-- ----------------------------
-- Records of notes
-- ----------------------------

-- ----------------------------
-- Table structure for praise_logs
-- ----------------------------
DROP TABLE IF EXISTS "main"."praise_logs";
CREATE TABLE "praise_logs" ("id" integer primary key autoincrement,"created_at" datetime,"updated_at" datetime,"deleted_at" datetime,"key" varchar(255),"user_id" integer,"type" varchar(255),"flag" bool );

-- ----------------------------
-- Records of praise_logs
-- ----------------------------

-- ----------------------------
-- Table structure for sqlite_sequence
-- ----------------------------
DROP TABLE IF EXISTS "main"."sqlite_sequence";
CREATE TABLE sqlite_sequence(name,seq);

-- ----------------------------
-- Records of sqlite_sequence
-- ----------------------------
INSERT INTO "main"."sqlite_sequence" VALUES ('users', 1);

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "main"."users";
CREATE TABLE "users" ("id" integer primary key autoincrement,"created_at" datetime,"updated_at" datetime,"deleted_at" datetime,"name" varchar(255),"email" varchar(255),"avatar" varchar(255),"pwd" varchar(255),"role" integer DEFAULT 0 );

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO "main"."users" VALUES (1, '2018-10-14 23:08:05.8009291+08:00', '2018-10-14 23:08:05.8009291+08:00', null, 'admin', 'admin@qq.com', '/static/images/info-img.png', 123123, 0);

-- ----------------------------
-- Indexes structure for table messages
-- ----------------------------
CREATE INDEX "main"."idx_messages_deleted_at"
ON "messages" ("deleted_at" ASC);
CREATE INDEX "main"."idx_messages_note_key"
ON "messages" ("note_key" ASC);
CREATE UNIQUE INDEX "main"."uix_messages_key"
ON "messages" ("key" ASC);

-- ----------------------------
-- Indexes structure for table notes
-- ----------------------------
CREATE INDEX "main"."idx_notes_deleted_at"
ON "notes" ("deleted_at" ASC);
CREATE UNIQUE INDEX "main"."uix_notes_key"
ON "notes" ("key" ASC);

-- ----------------------------
-- Indexes structure for table praise_logs
-- ----------------------------
CREATE INDEX "main"."idx_praise_logs_deleted_at"
ON "praise_logs" ("deleted_at" ASC);
CREATE INDEX "main"."idx_praise_logs_key"
ON "praise_logs" ("key" ASC);
CREATE INDEX "main"."idx_praise_logs_type"
ON "praise_logs" ("type" ASC);
CREATE INDEX "main"."idx_praise_logs_user_id"
ON "praise_logs" ("user_id" ASC);

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE INDEX "main"."idx_users_deleted_at"
ON "users" ("deleted_at" ASC);
CREATE UNIQUE INDEX "main"."uix_users_email"
ON "users" ("email" ASC);
CREATE UNIQUE INDEX "main"."uix_users_name"
ON "users" ("name" ASC);
